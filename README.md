A generic setup for preprocessing project files using Foundation.  To use 'livereload' download and install the [chrome extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)

## Installation ##

* bower install
* npm install

### IF you are receiving EACCESS errors, resolve using these methods: https://docs.npmjs.com/getting-started/fixing-npm-permissions

## When gulp can't find the correct version of SASS ##
* sudo gem install bundler
* sudo gem install sass -v [whichever version is in the Gemfile]

The bundler gem makes sure the version of foundation in this boilerplate is compatible with the sass gem on your machine.

## Tasks ##

**gulp watch** will watch all files, and run their associated tasks.

#### Styles ####
The "styles" task will compile all scss files into css, transform pixel values into rem units, prefix css3, and compile the result into htdocs/assets/styles/css.  gulp-ruby-sass requires the bundler gem to be installed to insure you are using the correct version of sass with the bower specified foundation version.


#### Scripts ####
The "scripts" task will uglify stand alone files such as modernizr, combine and uglify vendor scripts, and combine/uglify any scripts inside htdocs/assets/js/src


The boilerplate is set up to create the following scripts:

* /htdocs/assets/js/build/app.min.js
* /htdocs/assets/js/build/vendor/modernizr.js (which is minified)

#### Images ####
Minify and optimize images 


#### Jade (optional) ####
[Jade](http://jade-lang.com/) is a preprocessor for html.  Use it for rapid html development in the beginning of a project.  Jade source files are located in htdocs/jade, and compiled into htdocs/templates.  This keeps jade-compiled html files from interfering with the rest of the project, and insures other .html files with the same name won't get overwritten.  **All other markup files should be directly edited within the htdocs folder.**


## Configurable Paths ##
Paths can be configured for each task at the beginning of the gulp config file, in the paths object.