

$(document).ready(function() {
	$(this).foundation();
	
	$('.modal-trigger').on("click", function(){
	    $('.overlay').addClass('open');
	});

	$('.close').on("click", function(){
	    $('.overlay').removeClass('open');
	});

	$('.slick').slick({
		dots: true,
		infinite: true,
		speed: 700,
		fade: true,
		cssEase: 'linear',
		arrows: false
		//appendArrows: $('.slick-arrows'),
	});

	$('.slick-photos').slick({
		dots: true,
		infinite: true,
		speed: 700,
		fade: true,
		cssEase: 'linear',
		arrows: false
		//appendArrows: $('.slick-arrows'),
	});

/*! Fades out the whole page when clicking links 
	$('a').click(function(e) {
	e.preventDefault();
	newLocation = this.href;
	$('body').fadeOut('slow', newpage);
	});
	function newpage() {
	window.location = newLocation;
	}
*/
	return false;
});