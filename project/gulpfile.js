var bower_base = 'bower_components/';
var paths = {
	src: {
		styles: '../htdocs/assets/css/src/**/*.{css,scss}',
		jade: ['../htdocs/jade/**/*.jade'],
		scripts: [
				bower_base + 'jquery/dist/jquery.js',
				bower_base + 'foundation-sites/dist/js/foundation.js',

				// Plugins
				//bower_base + 'jquery-hoverIntent/jquery.hoverIntent.js',
				//bower_base + 'jquery-hoverIntent/jquery.hoverIntent.js',
				//bower_base + 'matchHeight/jquery.matchHeight.js',
				//bower_base + 'skrollr/dist/skrollr.min.js',
				bower_base + 'slick-carousel/slick/slick.min.js',
				//bower_base + 'vide/dist/jquery.vide.min.js',
				//bower_base + 'gsap/src/minified/jquery.gsap.min.js',
				//bower_base + 'gsap/src/minified/plugins/CSSPlugin.min.js',
				//bower_base + 'gsap/src/minified/easing/EasePack.min.js',
				//bower_base + 'gsap/src/minified/TimelineMax.min.js',
				//bower_base + 'gsap/src/minified/TweenMax.min.js',
				//bower_base + 'scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
				//bower_base + 'scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js',
				//bower_base + 'scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js',
				//bower_base + 'waypoints/lib/jquery.waypoints.min.js',
				//bower_base + 'flexslider/jquery.flexslider-min.js',
				//bower_base + 'parsleyjs/dist/parsley.min.js',
				//bower_base + 'fitvids/jquery.fitvids.js',

				'../htdocs/assets/js/src/*.js'
			],
		scss_paths: [
				bower_base + 'foundation-sites/scss'
		],
		scripts_vendor: ['../htdocs/assets/js/src/vendor/*.js']
	},
	build: {
		scripts: '../htdocs/assets/js/build',
		scripts_vendor: '../htdocs/assets/js/build/vendor',
		styles: '../htdocs/assets/css/build',
		jade: '../htdocs/templates'
	}
};

// Load plugin
var gulp = require('gulp'),
	jade = require('gulp-jade'),
	sass = require('gulp-ruby-sass'),
	prefix = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	cache = require('gulp-cache'),
	plumber = require('gulp-plumber'),
	livereload = require('gulp-livereload');


/******************************************
 * TASKS
 ******************************************/

// Jade
gulp.task('jade', function() {
	return gulp.src( paths.src.jade )
		.pipe( plumber() )
		.pipe( jade( { pretty: true } ) )
		.pipe( gulp.dest( paths.build.jade ))
		.pipe(livereload());
});

// Styles
gulp.task('styles', function() {
	return sass(paths.src.styles, {
			bundleExec: true,
			style: 'compressed',
			loadPath: paths.src.scss_paths
		})
		.on('error', sass.logError)
		.pipe(prefix({
			browsers: ['last 2 version', '> 1%'],
			cascade: true,
			remove: true
		}))
		.pipe( gulp.dest( paths.build.styles ) )
		.pipe(livereload());
});


// Scripts Concat and minify 
gulp.task('scripts-concat', function() {
	return gulp.src( paths.src.scripts )
		.pipe( plumber() )
		.pipe( uglify() )
		.pipe( concat('app.min.js') )
		.pipe( gulp.dest( paths.build.scripts ) );
});


// Scripts
// This task depends on `scripts-concat` to be finished first
gulp.task('scripts', ['scripts-concat'], function() {
  gulp.src(paths.src.scripts_vendor)
    .pipe(uglify())
    .pipe(gulp.dest(paths.build.scripts_vendor))
    .pipe(livereload());
});



gulp.task('reload', function() {
    return gulp.src([
        '../htdocs/**/*.{html,php}'
    ])
    .pipe(livereload());
});

gulp.task('default', function() {
	gulp.start('styles', 'scripts', 'jade');
});


/**
 *  Watch
 *
 *  Usage: gulp watch
 *  Watch watches for scss, coffee, image, and jade changes.
 *  
 */
gulp.task('watch', ['default'], function() {
	livereload.listen();

	// Watch jade files
	gulp.watch( paths.src.jade, ['jade'] );

	// Watch .scss files
	gulp.watch( paths.src.styles, ['styles'] );

	// Watch .js files
	gulp.watch( paths.src.scripts, ['scripts'] );

	// Watch markup file changes
	//gulp.watch( '../htdocs/**/*.{html,php}', ['reload']);

});